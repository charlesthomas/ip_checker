#!/usr/bin/perl
use strict;
use LWP::Simple;

my $file = '/home/crthomas/Dropbox/notes/IP.txt';
my $url  = 'http://automation.whatismyip.com/n09230945.asp';
open ( IN, $file )
	|| die $!;
chomp ( my @lines = <IN> );
close IN;

my $new_ip = get ( $url )
	|| die $!;

if ( $new_ip ne $lines[0] ) {
	my $timestamp = localtime ( time );

	open ( OUT, '>' . $file )
		|| die $!;
	print OUT "$new_ip\n";
	print OUT "---\n";
	print OUT "$timestamp\n";
	close OUT;
}
exit;
